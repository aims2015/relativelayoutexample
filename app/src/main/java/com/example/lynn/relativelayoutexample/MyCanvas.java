package com.example.lynn.relativelayoutexample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by lynn on 6/23/2015.
 */
public class MyCanvas extends View {
    private Paint paint;

    public MyCanvas(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFFFF0000);
    }

    public void onDraw(Canvas canvas) {
        canvas.drawRect(0,0,100,100,paint);
    }

}

package com.example.lynn.relativelayoutexample;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.relativelayoutexample.MainActivity.*;

/**
 * Created by lynn on 6/23/2015.
 */
public class MyView extends RelativeLayout {
    public MyView(Context context) {
        super(context);

        button1 = new Button(context);

        button2 = new Button(context);

        button1.setText("Button 1");

        button2.setText("Button 2");

        button1.setId(R.id.button1);

        RelativeLayout.LayoutParams layout1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layout1.addRule(RelativeLayout.RIGHT_OF,R.id.button1);

        button2.setLayoutParams(layout1);

        addView(button1);
        addView(button2);

        myCanvas = new MyCanvas(context);

        RelativeLayout.LayoutParams layout2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layout2.addRule(RelativeLayout.BELOW,R.id.button1);

        myCanvas.setLayoutParams(layout2);

        addView(myCanvas);


    }
}
